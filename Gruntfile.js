;
(__ => {
    'use strict';

    const _ = require('lodash');

    module.exports = function(grunt) {
        const data = grunt.file.readJSON('config.json');

        const sourceDirectory = data.sourceDirectory;
        const targetDirectory = data.targetDirectory;
        const toMinifyJsFileName = data.toMinifyJsFileName;
        const toMinifyCssFileName = data.toMinifyCssFileName;
        const isUglifiedJs = data.isUglifiedJs;

        let toMinifyJsFileList = data.toMinifyJsFileList;
        let toMinifyCssFileList = data.toMinifyCssFileList;

        let toMinifyJsFilePath = {}, toMinifyCssFilePath = {}, myTarget = {};

        let toDeleteMinifiedJsFile = _.map(toMinifyJsFileList, v => v = targetDirectory + v);
        let toDeleteMinifiedCssFile = _.map(toMinifyCssFileList, v => v = targetDirectory + v);

        toMinifyJsFileList = _.map(toMinifyJsFileList, v => v = targetDirectory + v);
        toMinifyCssFileList = _.map(toMinifyCssFileList, v => v = targetDirectory + v);

        toMinifyJsFilePath[targetDirectory + toMinifyJsFileName] = toMinifyJsFileList;
        toMinifyCssFilePath[targetDirectory + toMinifyCssFileName] = toMinifyCssFileList;

        let clean = {
            js: toDeleteMinifiedJsFile,
            css : toDeleteMinifiedCssFile
        };

        grunt.loadNpmTasks('grunt-contrib-copy');
        grunt.loadNpmTasks('grunt-contrib-htmlmin');
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.loadNpmTasks('grunt-contrib-cssmin');
        grunt.loadNpmTasks('grunt-contrib-clean');
        grunt.loadNpmTasks('grunt-replace');

        grunt.initConfig({
            copy: {
                main: {
                    files: [{
                            expand: true,
                            cwd: sourceDirectory,
                            src: ["*.*"],
                            dest: targetDirectory,
                            filter: 'isFile'
                        },

                        {
                            expand: true,
                            cwd: sourceDirectory,
                            src: ["**/*.*"],
                            dest: targetDirectory
                        },

                    ],
                },
            },

            htmlmin: {
                dist: {
                    options: {
                        removeComments: true,
                        collapseWhitespace: true
                    },
                    files: [{
                        expand: true,
                        cwd: sourceDirectory,
                        src: ['**/*.html'],
                        dest: targetDirectory,
                        filter: 'isFile',
                    }]
                }
            },

            uglify: {
                options: {
                    mangle: isUglifiedJs
                },
                my_target: {
                    files: toMinifyJsFilePath
                }
            },

            cssmin: {
                options: {
                    shorthandCompacting: false,
                    roundingPrecision: -1
                },
                target: {
                    files: toMinifyCssFilePath
                }
            },

            clean: clean,

            replace: {
                dist: {
                    options: {
                        patterns: [
                            { match: /<\/body>/g, replacement: '<script src="'+toMinifyJsFileName+'"></script></body>' },
                            { match: /<\/head>/g, replacement: '<link rel="stylesheet" href="'+toMinifyCssFileName+'"/></head>' }
                        ]
                    },
                    files: [{
                        expand: true,
                        flatten: true,
                        cwd: targetDirectory,
                        src: ['index.html'],
                        dest: targetDirectory
                    }]
                }
            }
        });

        grunt.registerTask('default', ['copy', 'htmlmin', 'uglify', 'cssmin', 'clean', 'replace']);
    }
})();
