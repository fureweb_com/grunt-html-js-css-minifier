# [HTML + Javascript + CSS source minifier for angular project] v0.0.4

## 개요
- 소스 디렉토리 하위의 모든 HTML 파일을 minify 시켜줍니다.
- 소스 디렉토리 내 지정한 모든 JS 파일을 minify+concat 시켜줍니다. **(선택적 uglify)**
- 소스 디렉토리 내 지정한 모든 css 파일을 minify+concat 시켜줍니다.
- **config.json** 파일 설정 후 `run.bat`파일을 실행시켜 위 **source -> target** 으로 가공된 파일이 복사되도록 할 수 있습니다.
- 복사 완료 후 targetDirectory의 index.html의 최하단에 minified된 자바스크립트 파일을 자동으로 삽입시켜 DOM에 로드시켜줍니다.
- 현재 windows에서만 테스트 완료된 상태입니다.

## config.json 설정 방법
- grunt 실행 전, 아래 6가지 항목을 설정합니다.
- **sourceDirectory** : 소스가 들어있는 기준 디렉토리 지정(String) `ex) c:/works/assets`
- **targetDirectory** : 타겟이 들어있는 기준 디렉토리 지정(String) `ex) c:/works/target`
- **toMinifyJsFileName** : minified된 자바스크립트 파일명 지정(String)  `ex) minified.js`
- **toMinifyJsFileList** : minify시킬 소스 하위 디렉토리에서의 자바스크립트 파일 목록. 순서대로 지정하면 순서대로 minified+concat 처리됩니다. `ex) [ "1.js", "2.js" , "**/*.js" ]`
- **toMinifyCssFileName** : minified된 css 파일명 지정(String)  `ex) minified.css`
- **toMinifyCssFileList** : minify시킬 소스 하위 디렉토리에서의 css 파일 목록. 순서대로 지정하면 순서대로 minified+concat 처리됩니다. `ex) [ "1.css", "2.css" , "**/*.css" ]`

## grunt 실행방법
- 만약 node가 설치되어있지 않았다면, node 최신버전부터 설치합니다.
- **config.json** / **Gruntfile.js** / **package.json** 파일을 **원하는** 폴더에 위치시킵니다. 명령프롬프트를 실행하여 **파일들이 위치된 곳으로 이동** 합니다.
- `npm install` 명령을 실행시켜 **package.json** 의 모듈들을 설치시킵니다.
- **config.json** 파일을 제대로 설정했는지 다시 한번 확인한 뒤, `grunt --force` 명령을 실행시켜 소스 다듬기 작업을 수행시킵니다.
- **targetDirectory** 에 정상적으로 복사가 되었는지 확인합니다.

## 실행 예제
[기본 디렉토리 설정]

1. 소스 디렉토리 : c:/works/assets (생성 후 소스 복사)
2. 타겟 디렉토리 : c:/works/target (생성할 필요 없음)

[시나리오]

1. 소스 디렉토리 안에 모든 HTML을 minify시키고
2. Javascript는 minify+concat시킨 뒤
3. 타겟 디렉토리의 index.html에 minified시킨 js 파일 로딩 스크립트를 자동으로 삽입시킨다.

[노드 설치]

1. https://nodejs.org 에서 Node.js 최신버전(6버전 대)을 다운로드 합니다.

[파일 복사]

1. c:/test 디렉토리를 생성한 뒤, 첨부파일을 c:/test 디렉토리에 압축해제 시켜준 뒤, config.json, Gruntfile.js, package.json, run.bat 총 4개의 파일이 존재하는지 확인합니다.
2. c:/works/assets 디렉토리를 생성한 뒤, 해당 위치에 minify 시킬 소스를 복사시켜놓습니다.

[config.json 설정]

1. 파일 내용을 아래처럼 설정합니다.
1) c:/works/assets/ 디렉토리를 소스 디렉토리로 지정합니다.
2) c:/works/target/ 디렉토리를 타겟 디렉토리로 지정합니다.
3) c:/works/target/minified.js 파일이 최종적으로 minified된 자바스크립트 파일이 될 것입니다.
4) toMinifyJsFileList에 concat 되어야 할 순서를 지정합니다. 아래의 내용은 다음의 순서를 가집니다.
- c:/works/target/lib/jquery.js 파일을 가장 먼저 minify시킵니다.
- c:/works/target/\*.js 파일을 minify시킵니다.
- c:/works/target/모든디렉토리/\*.js 파일을 minifiy 시킵니다.
5) c:/works/target/minified.css 파일이 최종적으로 minified된 CSS 파일이 될 것이고, js 파일과 마찬가지로 순서를 지정할 수 있습니다.
6) config.json의 isUglifiedJs 인덱스에 들어있는 boolean값을 통해 자바스크립트소스를 uglify까지 시켜줄 것인지 설정할 수 있습니다. Angular의 경우, 함수에 파라미터 형태로 서비스를 주입받게 되는데, 만약 uglify를 대비하지 않은 코드로 작성되어있는 경우 앵귤러 서비스 주입이 제대로 되지 않게 되므로 아래와 같은 옵션값을 통해 uglify를 선택적으로 수행할 수 있어야 합니다.
```
{
    "sourceDirectory" : "c:/works/assets/",
    "targetDirectory" : "c:/works/target/",
    "toMinifyJsFileName" : "minified.js",
    "toMinifyJsFileList" : [
        "lib/jquery.js",
        "*.js",
        "**/*.js"
    ],
    "toMinifyCssFileName" : "minified.css",
    "toMinifyCssFileList" : [
        "css/**/*.css"
    ],
    "isUglifiedJs" : false
}
```

[명령 프롬프트 실행]

1. c:/test/config.json 설정을 위 처럼 마친 경우, 다음의 명령을 실행합니다.
```
C:/test> npm install & run
```
2. package.json 파일에 기재된 의존성 관련 모듈들이 자동으로 설치되고, 설치가 완료되면 run.bat 스크립트를 실행시킵니다.
3. 스크립트의 실행이 정상적으로 수행 되었다면, c:/works/target 디렉토리 내부에 파일들이 정상적으로 생성되었는지 확인합니다.

## 변경이력
```
<v0.0.4>
- javascript 파일의 uglify 여부를 선택적으로 수행할 수 있도록 속성 config.json 외부로 추가

<v0.0.3>
- minified된 css 파일을 head 하단에 자동 추가하는 기능 추가.
- css minify를 위한 의존성 모듈 추가(grunt-contrib-cssmin)

<v0.0.2>
- minified된 파일을 index.html body 하단에 자동 추가하는 기능 추가.
- 의존성 모듈 추가(grunt-replace)

<v0.0.1>
- 최초 커밋
```
